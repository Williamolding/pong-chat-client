﻿namespace ChatClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ChatText = new System.Windows.Forms.RichTextBox();
            this.ClientText = new System.Windows.Forms.TextBox();
            this.Send = new System.Windows.Forms.Button();
            this.ConnectButton = new System.Windows.Forms.Button();
            this.Nickname = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Disconnect = new System.Windows.Forms.Button();
            this.Background = new System.Windows.Forms.PictureBox();
            this.PlayerOne = new System.Windows.Forms.PictureBox();
            this.PlayerTwo = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.P1Connect = new System.Windows.Forms.Button();
            this.P2Connect = new System.Windows.Forms.Button();
            this.Ball = new System.Windows.Forms.PictureBox();
            this.GameTimer = new System.Windows.Forms.Timer(this.components);
            this.Bottom = new System.Windows.Forms.PictureBox();
            this.Right = new System.Windows.Forms.PictureBox();
            this.Top = new System.Windows.Forms.PictureBox();
            this.left = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.Background)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerOne)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerTwo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ball)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Top)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.left)).BeginInit();
            this.SuspendLayout();
            // 
            // ChatText
            // 
            this.ChatText.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ChatText.Location = new System.Drawing.Point(796, 12);
            this.ChatText.Name = "ChatText";
            this.ChatText.ReadOnly = true;
            this.ChatText.Size = new System.Drawing.Size(544, 392);
            this.ChatText.TabIndex = 0;
            this.ChatText.Text = "";
            this.ChatText.TextChanged += new System.EventHandler(this.ChatText_TextChanged);
            // 
            // ClientText
            // 
            this.ClientText.Location = new System.Drawing.Point(797, 410);
            this.ClientText.Name = "ClientText";
            this.ClientText.Size = new System.Drawing.Size(543, 20);
            this.ClientText.TabIndex = 1;
            // 
            // Send
            // 
            this.Send.Location = new System.Drawing.Point(1347, 407);
            this.Send.Name = "Send";
            this.Send.Size = new System.Drawing.Size(75, 23);
            this.Send.TabIndex = 2;
            this.Send.Text = "Send";
            this.Send.UseVisualStyleBackColor = true;
            this.Send.Click += new System.EventHandler(this.Send_Click);
            // 
            // ConnectButton
            // 
            this.ConnectButton.Location = new System.Drawing.Point(1346, 57);
            this.ConnectButton.Name = "ConnectButton";
            this.ConnectButton.Size = new System.Drawing.Size(75, 23);
            this.ConnectButton.TabIndex = 3;
            this.ConnectButton.Text = "Connect";
            this.ConnectButton.UseVisualStyleBackColor = true;
            this.ConnectButton.Click += new System.EventHandler(this.ConnectButton_Click);
            // 
            // Nickname
            // 
            this.Nickname.Location = new System.Drawing.Point(1346, 31);
            this.Nickname.Name = "Nickname";
            this.Nickname.Size = new System.Drawing.Size(93, 20);
            this.Nickname.TabIndex = 4;
            this.Nickname.Text = "Guest";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1343, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Nickname:";
            // 
            // Disconnect
            // 
            this.Disconnect.Location = new System.Drawing.Point(1347, 87);
            this.Disconnect.Name = "Disconnect";
            this.Disconnect.Size = new System.Drawing.Size(75, 23);
            this.Disconnect.TabIndex = 6;
            this.Disconnect.Text = "Disconnect";
            this.Disconnect.UseVisualStyleBackColor = true;
            this.Disconnect.Click += new System.EventHandler(this.Disconnect_Click);
            // 
            // Background
            // 
            this.Background.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.Background.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Background.Location = new System.Drawing.Point(12, 35);
            this.Background.Name = "Background";
            this.Background.Size = new System.Drawing.Size(743, 345);
            this.Background.TabIndex = 7;
            this.Background.TabStop = false;
            // 
            // PlayerOne
            // 
            this.PlayerOne.BackColor = System.Drawing.SystemColors.Desktop;
            this.PlayerOne.Location = new System.Drawing.Point(731, 49);
            this.PlayerOne.Name = "PlayerOne";
            this.PlayerOne.Size = new System.Drawing.Size(16, 50);
            this.PlayerOne.TabIndex = 8;
            this.PlayerOne.TabStop = false;
            // 
            // PlayerTwo
            // 
            this.PlayerTwo.BackColor = System.Drawing.SystemColors.HotTrack;
            this.PlayerTwo.Location = new System.Drawing.Point(23, 49);
            this.PlayerTwo.Name = "PlayerTwo";
            this.PlayerTwo.Size = new System.Drawing.Size(16, 50);
            this.PlayerTwo.TabIndex = 9;
            this.PlayerTwo.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(373, 31);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(10, 349);
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // P1Connect
            // 
            this.P1Connect.Location = new System.Drawing.Point(680, 408);
            this.P1Connect.Name = "P1Connect";
            this.P1Connect.Size = new System.Drawing.Size(75, 23);
            this.P1Connect.TabIndex = 13;
            this.P1Connect.Text = "Player1";
            this.P1Connect.UseVisualStyleBackColor = true;
            this.P1Connect.Click += new System.EventHandler(this.P1Connect_Click);
            // 
            // P2Connect
            // 
            this.P2Connect.Location = new System.Drawing.Point(12, 407);
            this.P2Connect.Name = "P2Connect";
            this.P2Connect.Size = new System.Drawing.Size(75, 23);
            this.P2Connect.TabIndex = 14;
            this.P2Connect.Text = "Player2";
            this.P2Connect.UseVisualStyleBackColor = true;
            this.P2Connect.Click += new System.EventHandler(this.P2Connect_Click);
            // 
            // Ball
            // 
            this.Ball.BackColor = System.Drawing.Color.DarkRed;
            this.Ball.Location = new System.Drawing.Point(442, 190);
            this.Ball.Name = "Ball";
            this.Ball.Size = new System.Drawing.Size(14, 13);
            this.Ball.TabIndex = 15;
            this.Ball.TabStop = false;
            // 
            // GameTimer
            // 
            this.GameTimer.Enabled = true;
            this.GameTimer.Interval = 20;
            this.GameTimer.Tick += new System.EventHandler(this.GameTimer_Tick);
            // 
            // Bottom
            // 
            this.Bottom.Location = new System.Drawing.Point(12, 370);
            this.Bottom.Name = "Bottom";
            this.Bottom.Size = new System.Drawing.Size(743, 10);
            this.Bottom.TabIndex = 16;
            this.Bottom.TabStop = false;
            // 
            // Right
            // 
            this.Right.Location = new System.Drawing.Point(753, 31);
            this.Right.Name = "Right";
            this.Right.Size = new System.Drawing.Size(10, 348);
            this.Right.TabIndex = 17;
            this.Right.TabStop = false;
            // 
            // Top
            // 
            this.Top.Location = new System.Drawing.Point(12, 31);
            this.Top.Name = "Top";
            this.Top.Size = new System.Drawing.Size(743, 10);
            this.Top.TabIndex = 18;
            this.Top.TabStop = false;
            // 
            // left
            // 
            this.left.Location = new System.Drawing.Point(7, 30);
            this.left.Name = "left";
            this.left.Size = new System.Drawing.Size(10, 350);
            this.left.TabIndex = 19;
            this.left.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1459, 458);
            this.ControlBox = false;
            this.Controls.Add(this.left);
            this.Controls.Add(this.Top);
            this.Controls.Add(this.Right);
            this.Controls.Add(this.Bottom);
            this.Controls.Add(this.Ball);
            this.Controls.Add(this.P2Connect);
            this.Controls.Add(this.P1Connect);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.PlayerTwo);
            this.Controls.Add(this.PlayerOne);
            this.Controls.Add(this.Background);
            this.Controls.Add(this.Disconnect);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Nickname);
            this.Controls.Add(this.ConnectButton);
            this.Controls.Add(this.Send);
            this.Controls.Add(this.ClientText);
            this.Controls.Add(this.ChatText);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chat Client";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Background)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerOne)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerTwo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Ball)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Bottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Top)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.left)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox ChatText;
        private System.Windows.Forms.TextBox ClientText;
        private System.Windows.Forms.Button Send;
        private System.Windows.Forms.Button ConnectButton;
        private System.Windows.Forms.TextBox Nickname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Disconnect;
        private System.Windows.Forms.PictureBox Background;
        private System.Windows.Forms.PictureBox PlayerOne;
        private System.Windows.Forms.PictureBox PlayerTwo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button P1Connect;
        private System.Windows.Forms.Button P2Connect;
        private System.Windows.Forms.PictureBox Ball;
        private System.Windows.Forms.Timer GameTimer;
        private System.Windows.Forms.PictureBox Bottom;
        private System.Windows.Forms.PictureBox Right;
        private System.Windows.Forms.PictureBox Top;
        private System.Windows.Forms.PictureBox left;
    }
}

