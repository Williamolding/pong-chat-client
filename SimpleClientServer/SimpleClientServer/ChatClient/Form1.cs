﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

using System.Runtime.Serialization.Formatters.Binary;

namespace ChatClient
{
    public partial class Form1 : Form
    {
        
        delegate void SetTextCallback(string text);
        delegate void SetNumCallback(int num);
        private TcpClient _tcpClient;
        private NetworkStream _stream;

        public BinaryWriter _writer2;
        public BinaryReader _reader2;

        

        private StreamWriter _writer;
        private StreamReader _reader;
        Boolean bouncedown = true;
        Boolean bounceforward = true;
        Thread threadPSR;
        //Thread threadG;
        Boolean GameStart = false;
        Boolean P1 = false;
        Boolean P2 = false;
        Boolean player2fail = false;
        Boolean player1fail = false;
      
        
        int  ballX = 0;
        int ballY = 0;
        

        int P2Y = 0;

        int P1Y = 0;
       

        public Form1()
        {
            
            InitializeComponent();
            
            _tcpClient = new TcpClient();

            P1Y = PlayerOne.Location.Y;
             P2Y = PlayerTwo.Location.Y;
           ballX = Ball.Location.X;
             ballY = Ball.Location.Y;
        }

        public bool Connect(string hostname, int port)
        {
            
             try
            {
                _tcpClient.Connect(hostname, port);
                _stream = _tcpClient.GetStream();
                _writer = new StreamWriter(_stream, Encoding.UTF8);
                _reader = new StreamReader(_stream, Encoding.UTF8);
                
                _writer2 = new BinaryWriter(_stream, Encoding.UTF8);
                _reader2 = new BinaryReader(_stream, Encoding.UTF8);
                threadPSR = new Thread(new ThreadStart(ProcessServerResponse));
                threadPSR.Start();
                ChatText.Text = "CONNECTED..." + "\n";
                string textJoined = Nickname.Text + " JOINED CHAT" ;
                _writer.WriteLine(textJoined);

                _writer.Flush();
            }
            catch (Exception e)
            {
                
                ChatText.Text += "Exception: " + e.Message + "\n";
                return false;
            }

            return true;
        }
        
        
        public void AppendText(string str)
        {
          
            if (this.ChatText.InvokeRequired)
                {
                    SetTextCallback d = new SetTextCallback(AppendText);
                    this.Invoke(d, new object[] { str });
                }
                else

                {
                    if (str.Contains("INFO#123"))
                    {


                        if (P1 == true)
                        {
                            String source = str;
                            string extract = "";
                            int start = source.IndexOf("D") + 1;
                            int end = source.IndexOf("E");
                            if (start >= 0)
                            {
                                extract = source.Substring(start, end - start);
                                if (P2 == false)
                                {
                                    P2Y = Int32.Parse(extract);
                                }

                            }
                        }

                        if (P2 == true)
                        {
                            String source = str;
                            string extract = "";
                            int start = source.IndexOf("C") + 1;
                            int end = source.IndexOf("D");
                            if (start >= 0)
                            {
                                extract = source.Substring(start, end - start);
                                if (P1 == false)
                                {
                                    P1Y = Int32.Parse(extract);
                                }


                            }
                        }




                        if (P1 == false && P2 == false)
                        {
                            String source = str;
                            string extract = "";
                            int start = source.IndexOf("A") + 1;
                            int end = source.IndexOf("B");
                            if (start >= 0)
                            {
                                extract = source.Substring(start, end - start);
                                ballX = Int32.Parse(extract);
                            }


                            extract = "";
                            start = source.IndexOf("B") + 1;
                            end = source.IndexOf("C");
                            if (start >= 0)
                            {
                                extract = source.Substring(start, end - start);
                                ballY = Int32.Parse(extract);
                            }
                            extract = "";
                            start = source.IndexOf("C") + 1;
                            end = source.IndexOf("D");
                            if (start >= 0)
                            {
                                extract = source.Substring(start, end - start);
                                P1Y = Int32.Parse(extract);
                            }
                            extract = "";
                            start = source.IndexOf("D") + 1;
                            end = source.IndexOf("E");
                            if (start >= 0)
                            {
                                extract = source.Substring(start, end - start);
                                P2Y = Int32.Parse(extract);
                            }
                            extract = "";
                            start = source.IndexOf("E") + 1;
                            end = source.IndexOf("Z");
                            if (start >= 0)
                            {
                                extract = source.Substring(start, end - start);
                                int BF = Int32.Parse(extract);
                                if (BF == 1)
                                {
                                    bounceforward = true;
                                }
                                else if (BF == 0)
                                {
                                    bounceforward = false;
                                }



                            }
                            extract = "";
                            start = source.IndexOf("Z") + 1;
                            end = source.IndexOf("G");
                            if (start >= 0)
                            {
                                extract = source.Substring(start, end - start);
                                int BD = Int32.Parse(extract);
                                if (BD == 1)
                                {
                                    bouncedown = true;
                                }
                                else if (BD == 0)
                                {
                                    bouncedown = false;
                                }
                            }

                        }
                        //"E"  + bounceforward + "F" + bouncedown + "G";

                    }
                    else
                    {


                    ChatText.Text += str + "\n";
                    ChatText.SelectionStart = ChatText.Text.Length;
                    ChatText.ScrollToCaret();
                }

                }
           
        }

        public void GameActions(int num)
        {
            if (this.ChatText.InvokeRequired)
            {
                SetNumCallback d = new SetNumCallback(GameActions);
                this.Invoke(d, new object[] { num });
            }
            else
            {
                if (num == 4)
                {
                    P2Connect.Visible = true;
                    P2Connect.Enabled = true;

                }
                if (num == 3)
                {
                    P1Connect.Visible = true;
                    P1Connect.Enabled = true;

                }
                if (num== 2)
                {
                    P2Connect.Enabled = false;

                }
                if (num == 1)
                {
                    P1Connect.Enabled = false;

                }
              
            }
        }
        public void Run()
        {
            //if (!_tcpClient.Connected)
            //    throw new NotConnectedException();

            //try
            //{
            //    string userInput;

            //    ProcessServerResponse();
            //    ChatText.Text += "Enter the data to be sent: " + "\n";



            //    while ((userInput = Console.ReadLine()) != null)
            //    {
            //        _writer.WriteLine(userInput);
            //        _writer.Flush();

            //        ProcessServerResponse();

            //        if (userInput.Equals("9"))
            //            break;

            //        ChatText.Text += "Enter the data to be sent: " + "\n";
            //    }
            //}
            //catch (Exception f)
            //{
            //    ChatText.Text += "Exception: " + f.Message + "\n";
            //}
            //finally
            //{
            //    _tcpClient.Close();

            //}
            //Console.Read();



        }

        private void Form1_Load(object sender, EventArgs e)
        {
            


        }

        private void Send_Click(object sender, EventArgs e)
        {
            try
            {
                

                    string text = Nickname.Text + ":" + ClientText.Text;
                _writer.WriteLine(text);

                _writer.Flush();


            }
            catch (Exception j)
            {
                ChatText.Text += "Exception: " +j.Message + "\n";
             


            }
        }
        private void ProcessServerResponse()
        {

                
                while (true)
                        {
                string message = _reader.ReadLine();
                AppendText(message);

                            if (message.Contains("IS NOW PLAYER 2") && P2 == false)
                            {
                                GameActions(2);
                            }
                            if (message.Contains("IS NOW PLAYER 1") && P1 == false)
                            {
                                GameActions(1);
                            }

                            if (message.Contains("PLAYER 1 LOST"))
                            {
                                GameActions(3);
                            }
                            if (message.Contains("PLAYER 2 LOST"))
                            {
                                GameActions(4);
                            }


                        }
                        
                
            
        }
        
        private void ChatText_TextChanged(object sender, EventArgs e)
        {

        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            Connect("127.0.0.1", 4444);
            //check = true;
        }

        private void Disconnect_Click(object sender, EventArgs e)
        {
            
            string textleft = Nickname.Text + " LEFT CHAT";
            _writer.WriteLine(textleft);

            _writer.Flush();

            if (threadPSR.IsAlive)
            {
                threadPSR.Abort();
                this.Close();
            }
            
        }

        private void P2Connect_Click(object sender, EventArgs e)
        {
            if (_tcpClient.Connected)
            {
                if (P2 == false)
                {
                    P2 = true;
                    P2Connect.Enabled = false;
                    P1Connect.Visible = false;
                    string text = Nickname.Text + " IS NOW PLAYER 2";
                    _writer.WriteLine(text);

                    _writer.Flush();



                }
            }
        }

        private void P1Connect_Click(object sender, EventArgs e)
        {
            if (_tcpClient.Connected)
            {
                if (P1 == false)
                {
                    P1 = true;
                    P2Connect.Visible = false;
                    P1Connect.Enabled = false;
                    string text = Nickname.Text + " IS NOW PLAYER 1";
                    _writer.WriteLine(text);

                    _writer.Flush();



                }
            }
        }

        private void Game()
        {
            if (P1Connect.Enabled == false && P2Connect.Enabled == false && GameStart == false)
            {
                GameStart = true;
                if (P1 == true)
                {
                    string text = "GameStart";
                    _writer.WriteLine(text);

                    _writer.Flush();
                }
            }
            //if (ChatText.Text.Contains("GameStart"))
            //{
            //    //work on
            //    GameStart = true;
            //}

            if (GameStart == true)
            {


                int gravity = 3;
                int force = 6;
                int velocity = 0;
                int acceleration = 3;
                int Maxacceleration = 10;

                int BallSpeed = 10;
                int PaddleSpeed = 10;
                
                int ballW = Ball.Width;
                int ballH = Ball.Height;
                int ballWX = ballX + ballW;
                int ballHY = ballY + ballH;




                int topW = Top.Width;
                int topH = Top.Height;
                int topX = Top.Location.X;
                int topY = Top.Location.Y;
                int topWX = topX + topW;
                int topHY = topY + topH;

                int bottomW = Bottom.Width;
                int bottomH = Bottom.Height;
                int bottomX = Bottom.Location.X;
                int bottomY = Bottom.Location.Y;
                int bottomWX = bottomX + bottomW;
                int bottomHY = bottomY + bottomH;

                int rigthtW = Right.Width;
                int rigthH = Right.Height;
                int rigthX = Right.Location.X;
                int rigthY = Right.Location.Y;
                int rigthWX = rigthX + rigthtW;
                int rigthHY = rigthY + rigthH;

                int leftW = left.Width;
                int leftH = left.Height;
                int leftX = left.Location.X;
                int leftY = left.Location.Y;
                int leftWX = leftX + leftW;
                int leftHY = leftY + leftH;

                int borderW = Background.Width;
                int borderH = Background.Height;
                int borderX = Background.Location.X;
                int borderY = Background.Location.Y;
                int rectWX = borderX + borderW;
                int rectHY = borderY + borderH;


              
                
                int P2X = PlayerTwo.Location.X;
                int P2W = PlayerTwo.Width;
                int P2H = PlayerTwo.Height;
                int P2WX = P2X + P2W;
                int P2HY = P2Y + P2H;



                int P1X = PlayerOne.Location.X;
                
                int P1W = PlayerOne.Width;
                int P1H = PlayerOne.Height;
                int P1WX = P1X + P1W;
                int P1HY = P1Y + P1H;

                if (P1 == true && P2Connect.Enabled == false || P2 == true && P1Connect.Enabled == false)
                {
                    if (ballY <= topHY)
                    {
                        bouncedown = true;

                    }
                    if (ballHY >= bottomY)
                    {
                        bouncedown = false;
                    }
                    if (rigthX <= ballWX)
                    {
                         player1fail = true;
                         
                        //player 1 fail state
                        if (P1 == true)
                        {
                            
                            P1 = false;
                            
                            string player2fail = "PLAYER 1 LOST";
                            _writer.WriteLine(player2fail);

                            _writer.Flush();
                            GameStart = false;
                        }
                        if (P2 == true)
                        {
                            GameStart = false;
                        }

                            bounceforward = false;
                       
                    }
                    if (ballX <= leftWX)
                    {
                        player2fail = true;
                        if (P2 == true)
                        {
                            
                            P2 = false;
                            
                            string player2fail = "PLAYER 2 LOST";
                            _writer.WriteLine(player2fail);

                            _writer.Flush();
                            GameStart = false;
                        }
                        if (P1 == true)
                        {
                            GameStart = false;
                        }



                        bounceforward = true;
                       
                    }
                    //================



                    if (ballX < P1X + P1W && ballX + ballW > P1X && ballY < P1Y + P1H && ballH + ballY > ballY)
                    {
                        if (bouncedown == true)
                        {
                            bouncedown = false;
                        }
                        else
                        {
                            bouncedown = true;
                        }
                        if (bounceforward == true)
                        {
                            bounceforward = false;
                        }
                        else
                        {
                            bounceforward = true;
                        }
                    }
                    if (ballX < P2X + P2W && ballX + ballW > P2X && ballY < P2Y + P2H && ballH + ballY > ballY)
                    {
                        if (bouncedown == true)
                        {
                            bouncedown = false;
                        }
                        else
                        {
                            bouncedown = true;
                        }
                        if (bounceforward == true)
                        {
                            bounceforward = false;
                        }
                        else
                        {
                            bounceforward = true;
                        }
                    }


                    if (bounceforward == true)
                    {
                        ballX = ballX + acceleration;

                    }
                    if (bounceforward == false)
                    {
                        ballX = ballX - acceleration;

                    }
                    if (bouncedown == true)
                    {
                        ballY = ballY + acceleration;

                    }
                    if (bouncedown == false)
                    {
                        ballY = ballY - acceleration;

                    }

                }
                

                    Point C = Cursor.Position;
                Point Mp = this.PointToClient(C);
                int MY = Mp.Y;
                int MX = Mp.X;
                int P2M = (P2H - P2Y / 2);
              
                if (P2 == true)
                {

                    if (MX >= P2X && MX <= P2X + P2W && MY >= P2Y && MY <= P2Y + P2H)

                    {
                        if (P2Y > topY + topH && P2Y + P2H < bottomY)
                        {
                            P2Y = MY - 25;

                        }
                        else if (P2Y > topY + topH)
                        {
                            P2Y = P2Y - 2;
                        }
                        else if (P2Y + P2H < bottomY)
                        {
                            P2Y = P2Y + 2;

                        }
                    }


                    


                }
                if (P1 == true)
                {

                    if (MX >= P1X && MX <= P1X + P1W && MY >= P1Y && MY <= P1Y + P1H)

                    {
                        if (P1Y > topY + topH && P1Y + P1H < bottomY)
                        {
                            P1Y = MY - 25;

                        }
                        else if (P1Y > topY + topH)
                        {
                            P1Y = P1Y - 2;
                        }
                        else if (P1Y + P1H < bottomY)
                        {
                            P1Y = P1Y + 2;

                        }


                    }

                }

                
                PlayerTwo.Location = new Point(P2X, P2Y);
                PlayerOne.Location = new Point(P1X, P1Y);
                Ball.Location = new Point(ballX, ballY);
                if (P1 == true || P2 == true)
                {
                    int BF = 0;
                    int BD = 0;
                    if (bounceforward == true)
                    {
                        BF = 1;
                    }
                    else
                    {
                        BF = 0;
                    }
                    if (bouncedown == true)
                    {
                        BD = 1;
                    }
                    else
                    {
                        BD = 0;
                    }
                    string text = "INFO#123" + "A" + ballX + "B" + ballY + "C" + P1Y + "D" + P2Y + "E" + BF + "Z" + BD + "G";
                    _writer.WriteLine(text);

                    _writer.Flush();
                }
            }

        }

        private void GameTimer_Tick(object sender, EventArgs e)
        {
            Game();


        }

        private void PlayerOne_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void PlayerTwo_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {





        }
    }
    class NotConnectedException : Exception
    {
        public NotConnectedException() : base("TcpClient not connected.")
        { }

        public NotConnectedException(string message) : base(message)
        { }
    }
}
