﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatClient
{
    static class Program
    {
        private const string hostname = "127.0.0.1";
        private const int port = 4444;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Console.Read();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());


            Form1 _client = new Form1();

            if (_client.Connect(hostname, port))
            {
                Console.WriteLine("Connected...");

                try
                {
                    _client.Run();
                }
                catch (NotConnectedException e)
                {
                    Console.WriteLine("Client not Connected", e);
                }
            }
            else
            {
                Console.WriteLine("Failed to connect to: " + hostname + ":" + port);
            }

           
        }
    }
}
