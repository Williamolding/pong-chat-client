﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net.Sockets;
using System.Threading;
using System.IO;

using System.Runtime.Serialization.Formatters.Binary;

namespace SimpleServerCS
{
    public class Client
    {
        public Socket Socket;
        String ID;
        Thread thread;
        

        public NetworkStream stream;
        public StreamReader reader;
        public StreamWriter writer;
        public string NickName { get; private set; }
        public Client(Socket socket)
        {
            ID = Guid.NewGuid().ToString();
            Socket = socket;

            stream = new NetworkStream(socket, true);
            reader = new StreamReader(stream, Encoding.UTF8);
            writer = new StreamWriter(stream, Encoding.UTF8);


        }
        public void Start()
        {
            
            thread = new Thread(new ThreadStart(SocketMethod));
            thread.Start();
            


        }
        public void Stop()
        {

            Socket.Close();
            if (thread.IsAlive)
            {
                thread.Abort();
            }

        }
        public void SocketMethod()
        {
            SimpleServer.SocketMethod(Socket, (this));

        }
        public void SetNickName(string nickName)
        {
            this.NickName = nickName;
        }

      
        public void SendText(Client fromClient, String receivedMessage)
        {

            string Message = receivedMessage;
            //this.writer.WriteLine($"{ID}:{receivedMessage}");
            this.writer.WriteLine(receivedMessage);
            this.writer.Flush();



        }

    }
}
