﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;


namespace SimpleServerCS
{
    class SimpleServer
    {
        TcpListener _tcpListener;

        //Client list 
        static List<Client> _clients = new List<Client>();


        public SimpleServer(string ipAddress, int port)
        {
            IPAddress ip = IPAddress.Parse(ipAddress);
            _tcpListener = new TcpListener(ip, port);
        }

        public void Start()
        {
            _tcpListener.Start();
            
            Console.WriteLine("Listening...");
        
            
            //Console.WriteLine("Connection Made");
            //SocketMethod(socket);
            while (true)
            {
                Socket socket = _tcpListener.AcceptSocket();
                Client client = new Client(socket);
                _clients.Add(client);
                client.Start();

            };


            }

        public void Stop()
        {
            foreach (Client c in _clients)
            {
                _tcpListener.Stop();
            }
        }

        public static void SocketMethod(Socket socket, Client client)
        {
           
            try
            {
                 socket = client.Socket;




                string receivedMessage;
                NetworkStream stream = client.stream;
                StreamReader reader = client.reader;
                StreamWriter writer = client.writer;

                //writer.WriteLine("Send 0 for available options");
                //writer.Flush();
                
                
                while ((receivedMessage = reader.ReadLine()) != null)
                {
                    Console.WriteLine("Received...");

                    Console.WriteLine(receivedMessage);

                    foreach (Client c in _clients)
                    {
                        c.SendText(client,receivedMessage);
                    }



                   
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error occured: " + e.Message);
            }
            finally
            {
                //socket.Close();
                client.Stop();
            }
        }

        private static string GetReturnMessage(int code)
        {
            string returnMessage;

            switch (code)
            {
                case 0:
                    returnMessage = "Send 1, 3, 5 or 7 for a joke. Send 9 to close the connection.";
                    break;
                case 1:
                    returnMessage = "What dog can jump higher than a building? Send 2 for punchline!";
                    break;
                case 2:
                    returnMessage = "Any dog, buildings can't jump!";
                    break;
                case 3:
                    returnMessage = "When do Ducks wake up? Send 4 for punchline!";
                    break;
                case 4:
                    returnMessage = "At the Quack of Dawn!";
                    break;
                case 5:
                    returnMessage = "How do cows do mathematics? Send 6 for punchline!";
                    break;
                case 6:
                    returnMessage = "They use a cow-culator.";
                    break;
                case 7:
                    returnMessage = "How many programmers does it take to screw in a light bulb? Send 8 for punchline!";
                    break;
                case 8:
                    returnMessage = "None, that's a hardware problem.";
                    break;
                case 9:
                    returnMessage = "Bye!";
                    break;
                default:
                    returnMessage = "Invalid Selection";
                    break;
            }

            return returnMessage;
        }
    }
}
